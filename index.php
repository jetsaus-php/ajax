<!DOCTYPE html>
<html lang="ru">
<head>
    <title>Тест AJAX</title>
    <script src="js/jQuery.js"></script>
    <script>
        /*
         * Полный контроль
         */
        /*
        console.log(window.jQuery.fn.jquery);
        function funcBefore() {
            $("#information").text("Ожидание данных...");
        }
        function funcSuccess(data) {
            $("#information").text(data);
        }
        $(document).ready(function() {
           $("#load").bind("click", function () {
               var admin = "Admin";
               $.ajax ({
                   url: "content.php",
                   type: "POST",
                   data: ({name: admin, number: 5}),
                   dataType: "html",
                   beforeSend: funcBefore,
                   success: funcSuccess
               });
           });
        });
        */
        /*
         * Проверка логина
         */
        function funcBefore() {
            $("#information").text("Ожидание данных...");
        }
        function funcSuccess(data) {
            if (data == 'fail') {
                $("#information").text("Имя занято!");
            } else {
                $("#information").text(data);
            }
        }
        $(document).ready(function() {
            $("#done").bind("click", function () {
                $.ajax ({
                    url: "check.php",
                    type: "POST",
                    data: ({name: $("#name").val()}),
                    dataType: "html",
                    beforeSend: funcBefore,
                    success: funcSuccess
                });
            });
        });
    </script>
</head>
<body>
    <!--<p id="load" style="cursor:pointer">Загрузить данные</p>-->
    <input type="text" id="name" placeholder="введите имя">
    <input type="button" id="done" value="Готово">
    <div id="information"></div>
</body>
</html>